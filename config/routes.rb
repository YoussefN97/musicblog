Rails.application.routes.draw do


  scope "(:locale)", :locale => /en|de/ do
    # ***** Please put the routes here, so we can get a beautiful URL *****
    get 'items/edit'
    get 'items/index'
    get 'items/new'
    get 'items/show'
    get 'demo/view'
    get 'demo/artikel'
    get 'demo/datenschutz'
    get 'demo/galerie'
    get 'demo/hello'
    get 'demo/impressum'
    get 'demo/index'
    get 'demo/kontakt'
    get 'demo/konzerte'
    get 'demo/musik'
    get 'demo/shop'
    get 'demo/todo'


    get 'welcome/index'
    devise_for :users
    # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
    root 'welcome#index'

  end

  resources :items do
    member do
      patch :complete
    end
  end

  resources :concerts
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html


end
