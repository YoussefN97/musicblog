# Where the I18n library should search for translation files
I18n.load_path += Dir[Rails.root.join('config', 'locales', '**', '*.{rb,yml}').to_s]

# Permitted locales available for the application --> so far we're only using English and German
I18n.available_locales = [:en, :de]

# We use German as the standard language
I18n.default_locale = :de