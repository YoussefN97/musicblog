class CreateConcerts < ActiveRecord::Migration[6.0]
  def change
    create_table :concerts do |t|
      t.string :band
      t.string :place
      t.datetime :start_time

      t.timestamps
    end
  end
end
