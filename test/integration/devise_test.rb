require 'test_helper'

class DeviseTest < ActionDispatch::IntegrationTest
  # test "the truth" do
  #   assert true
  # end
  # German is the default language, so I added it to the path.

  test "should get login page" do
    get new_user_session_url
    assert_response :success
    assert_equal user_session_path, path
  end

  test "should get login page with post" do
    post new_user_session_url
    assert_response :success
    assert_equal user_session_path, path
  end

  test "should get sign up page" do
    get new_user_registration_url
    assert_response :success
    assert_equal new_user_registration_path, path
  end
end
