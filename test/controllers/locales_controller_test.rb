require 'minitest/autorun'
require 'test_helper'

class LocalesControllerTest < MiniTest::Unit::TestCase

  # I wanted to test something with a user, but it didn't work
  # Anyway, I wanted to keep the setup for future ideas.
  #def setup
  #  @user = User.new(email: 'John.Doe@example.com', encrypted_password: 'doe')
  #end

  def teardown
    # Nothing to tear down
  end

  def test_locale_set
    assert_equal I18n.default_locale, I18n.locale
  end

end