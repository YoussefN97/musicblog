require 'test_helper'

class DemoControllerTest < ActionDispatch::IntegrationTest
  test "should get view" do
    get demo_view_url
    assert_response :success
  end

  test "should get artikel" do
    get demo_artikel_url
    assert_response :success
  end

  test "should get datenschutz" do
    get demo_datenschutz_url
    assert_response :success
  end

  test "should get galerie" do
    get demo_galerie_url
    assert_response :success
  end

  test "should get hello" do
    get demo_hello_url
    assert_response :success
  end

  test "should get impressum" do
    get demo_impressum_url
    assert_response :success
  end

  test "should get index" do
    get demo_index_url
    assert_response :success
  end

  test "should get kontakt" do
    get demo_kontakt_url
    assert_response :success
  end

  test "should get konzerte" do
    get demo_konzerte_url
    assert_response :success
  end

  test "should get musik" do
    get demo_musik_url
    assert_response :success
  end

  test "should get shop" do
    get demo_shop_url
    assert_response :success
  end

  test "should get todo" do
    get demo_todo_url
    assert_response :success
  end

end
