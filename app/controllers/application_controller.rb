class ApplicationController < ActionController::Base

  # for changing the language
  protect_from_forgery

  before_action :set_locale

  # for the time in the calendar
  before_action :set_time_zone, if: :user_signed_in?

  private

  def set_time_zone
    Time.zone = current_user.time_zone
  end

  def set_locale
    I18n.locale = params[:locale] || I18n.default_locale
    Rails.application.routes.default_url_options[:locale] = I18n.locale
  end

end
