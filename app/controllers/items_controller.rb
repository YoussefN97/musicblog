class ItemsController < ApplicationController
  before_action :find_item, only: [:show, :edit, :update, :destroy, :complete]
  def index
     @items = Item.all.order("created_at DESC")
  end

  def new
    @item = Item.new
  end

  def create
    @item = Item.new(item_params)
    if @item.save
      @items = Item.all.order("created_at DESC")
      render 'items/index'
    else
      render 'new'
    end
  end

  def update
    if @item.update(item_params)
      redirect_to item_path(@item)
    else
      render 'edit'
    end
  end

  def destroy
    @item.destroy
    @items = Item.all.order("created_at DESC")
    render 'items/index'
  end

  def edit

  end

  def complete
    @item.update_attribute(:completed_at, Time.now)
    @items = Item.all.order("created_at DESC")
    render 'items/index'
  end

  def show

  end

  private

  def item_params
    params.require(:item).permit(:name, :description)
  end

  def find_item
    @item = Item.find(params[:id])
  end

end
