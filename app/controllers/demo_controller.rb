class DemoController < ApplicationController

  def artikel
  end

  def datenschutz
  end

  def galerie
  end

  def hello
  end

  def impressum
  end

  def index
  end

  def kontakt
  end

  def konzerte
  end

  def musik
  end

  def shop
  end

  def todo
    @items = Item.all.order("created_at DESC")
    render 'items/index'
  end
end
